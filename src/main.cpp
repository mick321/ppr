#include "stdafx.h"
#include "defaults.h"
#include "program.h"

// Program entry point.
int main(int argc, char* argv[])
{
#ifdef PPR_PARALLEL_MPI
  // initialize MPI
  if (MPI_SUCCESS != MPI_Init(&argc, &argv))
  {
    std::cerr << "Program failed to initialize MPI." << std::endl;
    return -1;
  }
#endif
  ppr::TResult res;
  ppr::Program program;

  // initialize instance of "main" class.
  if (argc == 3)
    res = program.Initialize(argv[1], argv[2]);
  else
    res = program.Initialize(ppr::PPR_CONFIG_FILE, ppr::PPR_DATA_FILE);
  if (res != ppr::resOk)
  {
    std::cerr << "Program failed to initialize. Number of error: " << res << std::endl;
    return res;
  }
  // run the computation
  res = program.Run();
  if (res != ppr::resOk)
  {
    std::cerr << "An error occurred during program run. Number of error: " << res << std::endl;
    return res;
  }
  // deinit the "main" class
  res = program.DeInitialize();
  if (res != ppr::resOk)
  {
    std::cerr << "An error occurred during program deinitialization. Number of error: " << res << std::endl;
    return res;
  }
  
#ifdef PPR_PARALLEL_MPI
  // finalize work with MPI
  MPI_Finalize();
#endif
	return 0;
}


#include "stdafx.h"
#include "program.h"
#include "computation.h"

namespace ppr
{
  // Called at each ini file statement recognition.
  int Config::callbackIniChunk(void* user, const char* section, const char* name, const char* value)
  {
    Config* _this = reinterpret_cast<Config*>(user);
    if (!strcmp(section, "Bounds")) // check section name
    {
      REAL valueD = (REAL)strtod(value, NULL); // convert value to REAL
      if (!strcmp(name, "pmin"))
        _this->pmin = valueD;
      else if (!strcmp(name, "pmax"))
        _this->pmax = valueD;
      else if (!strcmp(name, "cgmin"))
        _this->cgmin = valueD;
      else if (!strcmp(name, "cgmax"))
        _this->cgmax = valueD;
      else if (!strcmp(name, "cmin"))
        _this->cmin = valueD;
      else if (!strcmp(name, "cmax"))
        _this->cmax = valueD;
      else if (!strcmp(name, "ppmin"))
        _this->ppmin = valueD;
      else if (!strcmp(name, "ppmax"))
        _this->ppmax = valueD;
      else if (!strcmp(name, "cgpmin"))
        _this->cgpmin = valueD;
      else if (!strcmp(name, "cgpmax"))
        _this->cgpmax = valueD;
      else if (!strcmp(name, "cpmin"))
        _this->cpmin = valueD;
      else if (!strcmp(name, "cpmax"))
        _this->cpmax = valueD;
      else if (!strcmp(name, "dtmin"))
        _this->dtmin = valueD;
      else if (!strcmp(name, "dtmax"))
        _this->dtmax = valueD;
      else if (!strcmp(name, "hmin"))
        _this->hmin = valueD;
      else if (!strcmp(name, "hmax"))
        _this->hmax = valueD;
      else if (!strcmp(name, "kmin"))
        _this->kmin = valueD;
      else if (!strcmp(name, "kmax"))
        _this->kmax = valueD;
      else if (!strcmp(name, "mmin"))
        _this->mmin = valueD;
      else if (!strcmp(name, "mmax"))
        _this->mmax = valueD;
      else if (!strcmp(name, "nmin"))
        _this->nmin = valueD;
      else if (!strcmp(name, "nmax"))
        _this->nmax = valueD;
    }

    return 1;
  }

  // Constructor, "owner": pass responsible owner.
  Config::Config(Generic* owner) : Generic(owner)
  {
  }

  // Load from INI file.
  TResult Config::Load(const char* filename)
  {
    // put info to log
    char* charbuffer = (char*)malloc(strlen(filename) + 64); 
    if (charbuffer)
    {
      sprintf(charbuffer, "Loading configuration from ini file \"%s\"...", filename);
      static_cast<Program*>(mainProgram)->Log(charbuffer);
      free(charbuffer);
      charbuffer = NULL;
    }
    // start parsing ini file
    int err = ini_parse(filename, callbackIniChunk, this);
    if (err != 0)
    {
      if (err < 0)
      {
        char errstr[128];
        sprintf(errstr, "Failed to open configuration file %s", filename);
        static_cast<Program*>(mainProgram)->Log(errstr);
      }
      else
      {
        char errstr[128];
        sprintf(errstr, "Error in configuration file %s on line %d.", filename, err);
        static_cast<Program*>(mainProgram)->Log(errstr);
      }
      return resFailed;
    }
    // print "success" info to log
    static_cast<Program*>(mainProgram)->Log("Loading configuration finished successfully.");
    return resOk;
  }

  // ------------------------------------

  // Default constructor.
  Program::Program() : 
    Generic(NULL), 
    initialized(false),
    fileLog(NULL),
    config(this),
    data(this)
  {
  }

  // Default destructor. Unloads all data.
  Program::~Program()
  {
    DeInitialize(); // just in case, does nothing if already deinitialized
  }

  // Load and initialize all necessary data.
  TResult Program::Initialize(const char* filenameConfig, const char* filenameData)
  {
    if (initialized)
      return resOk;

    startTime = time(NULL);

    fileLog = fopen(PPR_LOG_FILE, "w");
    if (!fileLog)
    {
      fprintf(stderr, "Warning: Failed to open log file for writing (%s).", PPR_LOG_FILE);
    }

#ifdef PPR_PARALLEL_MPI
    int worldsize;
    MPI_Comm_size(MPI_COMM_WORLD, &worldsize);
    MPI_Comm_rank(MPI_COMM_WORLD, &myMPIRank);
    if (worldsize <= 1)
    {
      Log("This application requires at least two processors.");
      fileLog = NULL;
      return resFailed;
    }
#endif

    if (config.Load(filenameConfig) != resOk)
    {
      fclose(fileLog);
      fileLog = NULL;
      return resFailed;
    }

    if (data.Load(filenameData) != resOk)
    {
      fclose(fileLog);
      fileLog = NULL;
      return resFailed;
    }

    initialized = true;
    Log("Program initialization successful.");
    return resOk;
  }

  // Release all data used for computation.
  TResult Program::DeInitialize()
  {
    if (!initialized)
      return resOk;

    // release measured data
    data.Release();
    // release file descriptor (logging file)
    if (fileLog)
    {
      Log("Program is now being deinitialized.");
      fclose(fileLog);
      fileLog = NULL;
    }

    initialized = false;
    return resOk;
  }

  // Run the the whole program.
  TResult Program::Run()
  {
    if (!initialized)
      return resUninitialized;

    Log("Program - running...");

    bestResults.clear();
    countProcessed = 0;
    for (int m = (int)metricAbsDif; m < metricEnumCount; m++)
    {
      for (auto& seg : data.Segments())
      {
        char info[2048];

#ifdef PPR_PARALLEL_MPI
        if (myMPIRank == 0)
#endif
        {
          sprintf(info, "Segment id=%d, subject %s and metric %s.", seg.second.id, seg.second.subject.name.c_str(), GetMetricName((Metric)m));
          Log(info);
        }

#if !defined(PPR_PARALLEL_TBB) && !defined(PPR_PARALLEL_MPI)
        // serial code
        ComputationST computation(this, seg.second, (Metric)m);
        if (computation.Run() == resOk)
        {
          bestResults.push_back(computation.BestResult());
        }
#elif defined(PPR_PARALLEL_TBB)
        // parallel code using Intel TBB
        ComputationMT computation(this, seg.second, (Metric)m);
        if (computation.Run() == resOk)
        {
          bestResults.push_back(computation.BestResult());
        }
#elif defined(PPR_PARALLEL_MPI)
        // parallel code using MPI
        MPI_Barrier(MPI_COMM_WORLD); // sync before computation
        if (myMPIRank == 0)
        {
          // first node is master, exchanges best candidates between other nodes
          ComputationMPIMaster computation(this, seg.second, (Metric)m);
          if (computation.Run() == resOk)
          {
            bestResults.push_back(computation.BestResult());
          }
        }
        else
        {
          // other nodes are working slaves actually computing differential evolution (with hints from master)
          ComputationMPISlave computation(this, seg.second, (Metric)m);
          computation.Run();
        }
#endif

#ifdef PPR_PARALLEL_MPI
        MPI_Barrier(MPI_COMM_WORLD); // sync after computation, must be here because of "countProcessed++;"
        // receive delayed messages
        {
          std::uint8_t buffer[16 * 1024];
          int size, source;
          while (MPI_GenericReceive(this, buffer, 16 * 1024, size, source, false)) {}
        }
        if (myMPIRank == 0)
        {
          sprintf(info, "Segment id=%d, subject %s computation finished (metric %s).\n", seg.second.id, seg.second.subject.name.c_str(), GetMetricName((Metric)m));
          Log(info);
        }
#else
        sprintf(info, "Segment id=%d, subject %s computation finished (metric %s).\n", seg.second.id, seg.second.subject.name.c_str(), GetMetricName((Metric)m));
        Log(info);
#endif
        countProcessed++; // increase computation identifier
      }
    }

#ifdef PPR_PARALLEL_MPI
    if (myMPIRank == 0)
#endif
    {
      PrintThreeBestResults();

      char buffer[256];
      sprintf(buffer, "Program - finished (after %d seconds).", (int)(time(NULL) - startTime));
      Log(buffer);
    }
    return resOk;
  }

  // Write string "str" to log file. Any failure is silently ignored.
  // Parameter "printOnScreen" - print str on screen.
  // Parameter "printToFile" - print str to log file.
  // Parameter "flush" forces to flush buffers.
  void Program::Log(const char* str, const bool printOnScreen, const bool printToFile, const bool flush)
  {
#ifndef PPR_PARALLEL_MPI
    if (!fileLog)
      return;
    if (printOnScreen)
      printf("%s\n", str);
    if (printToFile)
    {
      fprintf(fileLog, "%s\n", str);
      if (flush)
        fflush(fileLog);
    }
#else
    if (printOnScreen)
      printf("P%d: %s\n", myMPIRank, str);

    if (printToFile)
    {
      if (myMPIRank == 0)
      {
        if (!fileLog)
          return;
        fprintf(fileLog, "%s\n", str);
        if (flush)
          fflush(fileLog);
      }
      else
      {
        // resend it to master
        int stringLen = (int)strlen(str);
        std::uint8_t* buffer = (std::uint8_t*)malloc(stringLen + 1 + 4 + 2);
        *((std::int32_t*)&buffer[0]) = PPR_MPI_MSG_LOG;
        buffer[4] = 0; // do not print on master screen
        buffer[5] = 1; // print to file
        memcpy(buffer + 6, str, stringLen + 1); // string len plus ending zero
        MPI_Send(buffer, stringLen + 1 + 4 + 2, MPI_BYTE, 0, PPR_MPI_TAG, MPI_COMM_WORLD); // send!
        free(buffer);
      }
    }
#endif
  }

  // Prints three best results of computation (based on value of fitness function).
  void Program::PrintThreeBestResults()
  {
    if (bestResults.size() < 3)
      return;
    REAL temporary;
    Candidate* threeBest[3] = { &bestResults[0], &bestResults[1], &bestResults[2] };
    temporary = DBL_MAX;
    for (Candidate& cand : bestResults)
      if (cand.fitness < temporary)
      {
        threeBest[0] = &cand;
        temporary = cand.fitness;
      }

    temporary = DBL_MAX;
    for (Candidate& cand : bestResults)
      if (cand.fitness < temporary && cand.fitness > threeBest[0]->fitness)
      {
        threeBest[1] = &cand;
        temporary = cand.fitness;
      }

    temporary = DBL_MAX;
    for (Candidate& cand : bestResults)
      if (cand.fitness < temporary && cand.fitness > threeBest[0]->fitness && cand.fitness > threeBest[1]->fitness)
      {
        threeBest[2] = &cand;
        temporary = cand.fitness;
      }

    Log("Three best results (best fitness function):");
    threeBest[0]->PrintCandidateInfo(this);
    threeBest[1]->PrintCandidateInfo(this);
    threeBest[2]->PrintCandidateInfo(this);
  }

#ifdef PPR_PARALLEL_MPI
  // Default MPI message handling.
  // returns true if handled successfully.
  bool Program::DefaultMessageHandler(const std::uint8_t* buffer, const int source)
  {
    std::int32_t messageIdentifier = *((std::int32_t*)&buffer[0]);
    switch (messageIdentifier)
    {
      return false;
    case PPR_MPI_MSG_FINISHED:
      sprintf((char*)buffer, "Slave %d ran out of iterations and finished.", source);
      static_cast<Program*>(mainProgram)->Log((char*)buffer);
      return false;
    case PPR_MPI_MSG_LOG:
      {
        // accept remote logs
        bool printOnScreen = (buffer[4] != 0);
        bool printToFile = (buffer[5] != 0);
        char* str = (char*)malloc(32 + strlen((char*)(buffer + 6)));
        sprintf(str, "Slave %d: %s", source, (char*)(buffer + 6));
        static_cast<Program*>(mainProgram)->Log(str, printOnScreen, printToFile, printToFile);
        free(str);
      }
      return true;
    default:
      // unrecognized
      return false;
    }
  }
#endif
}


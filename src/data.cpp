// data.cpp - representation of data for computation

#include "stdafx.h"
#include "data.h"
#include "program.h"

namespace ppr
{
  // Prepare spline interpolation of dataset.
  TResult MeasureSegment::PrepareSplines()
  {
    // ensure that spline classes are empty
    valuesBlood = mathutils::Spline1D<REAL>();
    valuesIst = mathutils::Spline1D<REAL>();
    for (const auto& p : points)
    {
      if (p.bloodNotNull)
      {
        if (valuesBlood.stored_values.size() == 0 || fabs(valuesBlood.stored_values.back().x - (REAL)p.tim) > PPR_EPS)
          valuesBlood.AddPoint((REAL)p.tim, p.blood);
      }
      if (p.istNotNull)
      {
        if (valuesIst.stored_values.size() == 0 || fabs(valuesIst.stored_values.back().x - (REAL)p.tim) > PPR_EPS)
          valuesIst.AddPoint((REAL)p.tim, p.ist);
      }
    }
    // we do not want "wild" behavior when extrapolating, third polynomial is a beast
    valuesBlood.extrapolationIsConstant = true;
    valuesIst.extrapolationIsConstant = true;
    // prepare C2
    valuesBlood.PrepareC2();
    valuesIst.PrepareC2();
    return resOk;
  }

  // --------------------------------------------------------------------------

  // Constructor, "owner" is a pointer to responsible owner.
  Data::Data(Generic* owner) : Generic(owner), initialized(false)
  {
  }

  int Data::LoadRawDataCallbackSt(void* userData, int columnCount, char** columnValues, char** columnNames)
  {
    // get context of our instance...
    return reinterpret_cast<Data*>(userData)->LoadRawDataCallback(columnCount, columnValues, columnNames);
  }

  // Callback defined for sqlite3_exec. Here we take care of each row fetched from db.
  int Data::LoadRawDataCallback(int columnCount, char** columnValues, char** columnNames)
  {
    // build measure point
    MeasurePoint point;
    for (int i = 0; i < columnCount; i++)
    {
      const char* columnValue = columnValues[i];
      const char* columnName = columnNames[i];
      if (!strcmp(columnName, PPR_SQLCOL_ID))
      {
        point.id = columnValue ? strtol(columnValue, NULL, 10) : -1;
      }
      else if (!strcmp(columnName, PPR_SQLCOL_MEASUREDAT))
      {
        point.tim = columnValue ? StrToDateTime(columnValue) : DATETIME(0);
      }
      else if (!strcmp(columnName, PPR_SQLCOL_BLOOD))
      {
        point.blood = columnValue ? strtod(columnValue, NULL) : 0;
        point.bloodNotNull = (columnValue != NULL);
      }
      else if (!strcmp(columnName, PPR_SQLCOL_IST))
      {
        point.ist = columnValue ? strtod(columnValue, NULL) : 0;
        point.istNotNull = (columnValue != NULL);
      }
      else if (!strcmp(columnName, PPR_SQLCOL_SEGMENTID))
      {
        point.segmentId = columnValue ? strtol(columnValue, NULL, 10) : -1;
      }
    }
    // store fetched data
    if (point.id != -1 && point.segmentId != -1)
    {
      MeasureSegment& addingToSegment = segments[point.segmentId];
      addingToSegment.id = point.segmentId;
      addingToSegment.points.push_back(point);
    }
    return 0;
  }

  // Callback defined for sqlite3_exec (static dispatcher), loading additional data about segments and subjects. Here we take care of each row fetched from db.
  int Data::LoadSubSegCallbackSt(void* userData, int columnCount, char** columnValues, char** columnNames)
  {
    // get context of our instance...
    return reinterpret_cast<Data*>(userData)->LoadSubSegCallback(columnCount, columnValues, columnNames);
  }

  // Callback defined for sqlite3_exec, loading additional data about segments and subjects. Here we take care of each row fetched from db.
  int Data::LoadSubSegCallback(int columnCount, char** columnValues, char** columnNames)
  {
    std::string segmentname;
    MeasureSubject subject;
    for (int i = 0; i < columnCount; i++)
    {
      const char* columnValue = columnValues[i];
      const char* columnName = columnNames[i];
      if (!strcmp(columnName, PPR_SQLCOL_ID))
      {
        subject.id = columnValue ? strtol(columnValue, NULL, 10) : -1;
      }
      else if (!strcmp(columnName, PPR_SQLCOL_SEGMENTID))
      {
        subject.segmentId = columnValue ? strtol(columnValue, NULL, 10) : -1;
      }
      else if (!strcmp(columnName, PPR_SQLCOL_SEGNAME))
      {
        segmentname = columnValue ? columnValue : "";
      }
      else if (!strcmp(columnName, PPR_SQLCOL_SUBNAME))
      {
        subject.name = columnValue ? columnValue : "";
      }
      else if (!strcmp(columnName, PPR_SQLCOL_SUBCOMMENTS))
      {
        subject.comments = columnValue ? columnValue : "";
      }
      else if (!strcmp(columnName, PPR_SQLCOL_SUBSEX))
      {
        subject.sex = columnValue ? columnValue : "";
      }
      else if (!strcmp(columnName, PPR_SQLCOL_SUBWEIGHT))
      {
        subject.weight = columnValue ? strtod(columnValue, NULL) : 0.0;
      }
    }
    if (subject.id != -1 && subject.segmentId != -1)
    {
      MeasureSegment& addingToSegment = segments[subject.segmentId];
      addingToSegment.name = segmentname;
      addingToSegment.subject = subject;
    }
    return 0;
  }

  // Load raw data (measurements) from sqlite db.
  TResult Data::LoadRawData(const char* filename)
  {  
    TResult rtnRes = resOk;
    // open database
    sqlite3* dblink = NULL;
    int errNumber;
    if ((errNumber = sqlite3_open_v2(filename, &dblink, SQLITE_OPEN_READONLY, NULL)) != 0)
    {
      static_cast<Program*>(mainProgram)->Log("Loading data failed:");
      static_cast<Program*>(mainProgram)->Log(sqlite3_errstr(errNumber));
      return resFailed;
    }

    // execute query
    char* errmsg = NULL;
    char* errmsg2 = NULL;
    if (sqlite3_exec(dblink, PPR_SQL_MEASUREDAT, LoadRawDataCallbackSt, this, &errmsg) != SQLITE_OK ||
      sqlite3_exec(dblink, PPR_SQL_SEGMSUBJDAT, LoadSubSegCallbackSt, this, &errmsg2) != SQLITE_OK)
    {
      rtnRes = resFailed;
    }
    // cleanup
    if (errmsg)
    {
      sqlite3_free(errmsg);
      errmsg = NULL;
    }
    // cleanup
    if (errmsg2)
    {
      sqlite3_free(errmsg2);
      errmsg2 = NULL;
    }

    // close database
    sqlite3_close_v2(dblink);
    return rtnRes;
  }

  // Load data from db file.
  TResult Data::Load(const char* filename)
  {
    if (initialized)
      Release();

    // put info to log
    char* charbuffer = (char*)malloc(strlen(filename) + 64);
    if (charbuffer)
    {
      sprintf(charbuffer, "Loading data from sqlite file \"%s\"...", filename);
      static_cast<Program*>(mainProgram)->Log(charbuffer);
      free(charbuffer);
      charbuffer = NULL;
    }

    // load measurements from db
    if (LoadRawData(filename) != resOk)
    {
      static_cast<Program*>(mainProgram)->Log("Loading measurement data failed.");
      return resFailed;
    }

    static_cast<Program*>(mainProgram)->Log("Constructing splines...");
    // construct splines from raw data
    for (auto& segment : segments)
    {
      segment.second.PrepareSplines();
    }

    // print "success" info to log
    static_cast<Program*>(mainProgram)->Log("Loading data finished successfully.");
    initialized = true; // tag this object as valid
    return resOk;
  }

  // Release all loaded data.
  TResult Data::Release()
  {
    if (!initialized)
      return resOk;

    initialized = false; // tag this object as invalid
    return resOk;
  }
}
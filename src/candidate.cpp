#include "stdafx.h"
#include "candidate.h"
#include "program.h"

namespace ppr
{
  // Print results - best candidate parameters.
  void Candidate::PrintCandidateInfo(Program* mainProgram) const
  {
    HARD_ASSERT(mainProgram, "PrintCandidateInfo: parameter mainProgram must not be NULL.");
    const Candidate& cand = *this;
    char buffer[512];
    sprintf(buffer, "p=%lf pp=%lf cg=%lf cgp=%lf", cand.p, cand.pp, cand.cg, cand.cgp);
    mainProgram->Log(buffer);
    sprintf(buffer, "c=%lf cp=%lf dt=%lf h=%lf", cand.c, cand.cp, cand.dt, cand.h);
    mainProgram->Log(buffer);
    sprintf(buffer, "k=%lf m=%lf n=%lf", cand.k, cand.m, cand.n);
    mainProgram->Log(buffer);
    sprintf(buffer, "... fitness=%le", cand.fitness);
    mainProgram->Log(buffer);
  }
}
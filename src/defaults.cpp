// defaults.cpp - definition of values of (extern) global constants, enums and structures
#include "stdafx.h"
#include "defaults.h"
#include "program.h"

namespace ppr
{
  // paths
  const char PPR_CONFIG_FILE[] = "../data/bounds.ini";
  const char PPR_DATA_FILE[] = "../data/data.sqlite3";
  const char PPR_LOG_FILE[] = "app_log.log";

  // SQL for fetching data from sqlite db
  const char PPR_SQL_MEASUREDAT[] =
    " SELECT"
    "   id, measuredat, blood, ist, segmentid"
    " FROM"
    "   measuredvalue"
    " ORDER BY"
    "   segmentid, measuredat";

  // SQL for fetching additional data (about segment and subject) from sqlite db
  const char PPR_SQL_SEGMSUBJDAT[] =
    " SELECT"
    "   subject.id AS id, timesegment.id AS segmentid, timesegment.name AS segmentname, subject.name AS subjectname, "
    "   subject.comments AS subjectcomments, subject.sex AS subjectsex, subject.weight AS subjectweight"
    " FROM"
    "   timesegment, subject"
    " WHERE"
    "   timesegment.subjectid = subject.id";

  // Definition of column names in sqlite db
  const char PPR_SQLCOL_ID[] = "id";
  const char PPR_SQLCOL_MEASUREDAT[] = "measuredat";
  const char PPR_SQLCOL_BLOOD[] = "blood";
  const char PPR_SQLCOL_IST[] = "ist";
  const char PPR_SQLCOL_SEGMENTID[] = "segmentid";
  const char PPR_SQLCOL_SEGNAME[] = "segmentname";
  const char PPR_SQLCOL_SUBNAME[] = "subjectname";
  const char PPR_SQLCOL_SUBCOMMENTS[] = "subjectcomments";
  const char PPR_SQLCOL_SUBSEX[] = "subjectsex";
  const char PPR_SQLCOL_SUBWEIGHT[] = "subjectweight";

  // threshold for fitness function - if fitness is lowerthan this value, then computation is considered finished
  const REAL PPR_FITNESS_THRESHOLD = 1e-40;
  // definition of epsilon number (small threshold, all number less than this are considered to be zero)
  const REAL PPR_EPS = 1e-30;

#ifdef PPR_PARALLEL_MPI
  // Generic message receiving (blocking, receives whole message). Returns true on success.
  bool MPI_GenericReceive(Generic* mainProgram, void* buffer, int maxSize, int& outSize, int& source, bool blocking)
  {
    MPI_Status status;
    // test for message
    int flag;
    if (blocking)
      MPI_Probe(MPI_ANY_SOURCE, PPR_MPI_TAG, MPI_COMM_WORLD, &status);
    else
      MPI_Iprobe(MPI_ANY_SOURCE, PPR_MPI_TAG, MPI_COMM_WORLD, &flag, &status);

    if (!blocking && !flag)
    {
      return false; // there is no message in queue
    }
    if (status.MPI_ERROR)
    {
      static_cast<Program*>(mainProgram)->Log("Error during message probing.");
      return false;
    }
    // get number of received bytes
    int numBytes = 0;
    MPI_Get_count(&status, MPI_BYTE, &numBytes);
    if (numBytes > maxSize || numBytes <= 0)
    {
      sprintf((char*)buffer, "Invalid message size (%d bytes) from slave %d.", numBytes, status.MPI_SOURCE);
      static_cast<Program*>(mainProgram)->Log((char*)buffer);
      return false;
    }

    source = status.MPI_SOURCE;
    MPI_Recv(buffer, numBytes, MPI_BYTE, source, PPR_MPI_TAG, MPI_COMM_WORLD, &status);
    if (status.MPI_ERROR)
    {
      sprintf((char*)buffer, "Error when trying to read message from slave %d.", source);
      static_cast<Program*>(mainProgram)->Log((char*)buffer);
      return false;
    }
    outSize = numBytes;
    return true;
  }
#endif
}
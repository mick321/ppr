// defaults.h - definition of global constants, enums and structures
#pragma once

namespace ppr
{
  enum TResult
  {
    resOk = 0,
    resFailed,
    resUninitialized
  };

  // Path to config
  extern const char PPR_CONFIG_FILE[];
  // Path to data (sqlite db)
  extern const char PPR_DATA_FILE[];
  // Path to output log
  extern const char PPR_LOG_FILE[];

  // SQL for fetching data from sqlite db
  extern const char PPR_SQL_MEASUREDAT[];
  // SQL for fetching additional data (about segment and subject) from sqlite db
  extern const char PPR_SQL_SEGMSUBJDAT[];

  // Definition of column names in sqlite db
  extern const char PPR_SQLCOL_ID[];
  extern const char PPR_SQLCOL_MEASUREDAT[];
  extern const char PPR_SQLCOL_BLOOD[];
  extern const char PPR_SQLCOL_IST[];
  extern const char PPR_SQLCOL_SEGMENTID[];
  extern const char PPR_SQLCOL_SEGNAME[];
  extern const char PPR_SQLCOL_SUBNAME[];
  extern const char PPR_SQLCOL_SUBCOMMENTS[];
  extern const char PPR_SQLCOL_SUBSEX[];
  extern const char PPR_SQLCOL_SUBWEIGHT[];

  // define our "double" type - to easily change it to more coarse/fine precision
  typedef double REAL;
  // define datetime - it is just double, where integral part means days and fractional part means time of the day
  typedef double DATETIME;

  // threshold for fitness function - if fitness is lowerthan this value, then computation is considered finished
  extern const REAL PPR_FITNESS_THRESHOLD;
  // definition of epsilon number (small threshold, all number less than this are considered to be zero)
  extern const REAL PPR_EPS;

#ifdef PPR_PARALLEL_MPI

// magic number (date of implementation)
#define PPR_MPI_TAG 141227

#define PPR_MPI_MSG_LOG        1
#define PPR_MPI_MSG_BEST       2
#define PPR_MPI_MSG_FINISHED   3

#endif

  // Conversion of string to datetime
  static inline DATETIME StrToDateTime(const char* str)
  {
    const long long int diffFrom1970To1900 = 2209161600;
    const double SecsPerDay = 24.0*60.0*60.0;
    const double InvSecsPerDay = 1.0 / SecsPerDay;
    // parse string
    int year, month, day, hour, minute, second;
    sscanf(str, "%d-%d-%dT%d:%d:%d", &year, &month, &day, &hour, &minute, &second);
    // fill the tm struct
    tm t; 
    t.tm_year = year - 1900;
    t.tm_mon = month - 1;
    t.tm_mday = day;
    t.tm_hour = hour;
    t.tm_min = minute;
    t.tm_sec = second;
    // return the result
    return ((long long int)mktime(&t) + diffFrom1970To1900) * InvSecsPerDay;
  }

  // return random real number from given interval using given random number generator
  static inline double RandReal(std::mt19937& random, const double low, const double high)
  {
    std::uniform_real_distribution<double> distribution(low, high);
    return distribution(random);
  }

  // Generic ancestor with simple linkage to owner.
  class Generic
  {
  protected:
    Generic* owner;
    Generic* mainProgram;
  public:
    // Basic constructor.
    // owner: pass pointer to responsible owner.
    Generic(Generic* owner)
    {
      this->owner = owner;
      this->mainProgram = owner ? owner->mainProgram : this;
    }
  };

#ifdef PPR_PARALLEL_MPI
  // Generic message receiving (blocking, receives whole message). Returns true on success.
  bool MPI_GenericReceive(Generic* mainProgram, void* buffer, int maxSize, int& outSize, int& source, bool blocking);
#endif
}
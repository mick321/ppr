// Program.h - definition of main program class
#pragma once
#include "defaults.h"
#include "data.h"
#include "candidate.h"

namespace ppr
{
  // Class containing configuration of program.
  class Config : public Generic
  {
  private:
    // Called at each ini file statement recognition.
    static int callbackIniChunk(void* user, const char* section, const char* name, const char* value);
  public:
    // Constants used in computation - these are loaded from configuration ini file.
    // At this moment, these contants are treated as "magic numbers in computation".
    REAL pmin;
    REAL pmax;
    REAL cgmin;
    REAL cgmax;
    REAL cmin;
    REAL cmax;

    REAL ppmin;
    REAL ppmax;
    REAL cgpmin;
    REAL cgpmax;
    REAL cpmin;
    REAL cpmax;

    REAL dtmin;
    REAL dtmax;
    REAL hmin;
    REAL hmax;

    REAL kmin;
    REAL kmax;

    REAL mmin;
    REAL mmax;
    REAL nmin;
    REAL nmax;

    // Constructor, "owner": pass responsible owner.
    Config(Generic* owner);
    // Load from INI file.
    TResult Load(const char* filename);
  };

  // Main program class, defines flow of computation.
  class Program : public Generic
  {
  private:
    bool initialized;  // is this class properly initialized?
    FILE* fileLog;     // file descriptor - logging
    std::vector<Candidate> bestResults; // vector of best results
    time_t startTime;

    // Prints three best results of computation (based on value of fitness function).
    void PrintThreeBestResults();

#ifdef PPR_PARALLEL_MPI
    int myMPIRank; // flag used for synchronization between processes
#endif
  public:
    Config config;     // configuration file (with bounds)
    Data data;         // preprocessed data loaded from sqlite file

    int countProcessed; // count of already processed segments

    // Default constructor.
    Program();
    // Default destructor. Unloads all data.
    virtual ~Program();

    // Load and initialize all necessary data.
    TResult Initialize(const char* filenameConfig, const char* filenameData);
    // Release all data used for computation.
    TResult DeInitialize();

    // Run the the whole program.
    TResult Run();
    // Write string "str" to log file. Any failure is silently ignored.
    // Parameter "printOnScreen" - print str on screen.
    // Parameter "printToFile" - print str to log file.
    // Parameter "flush" forces to flush buffers.
    void Log(const char* str, const bool printOnScreen = true, const bool printToFile = true, const bool flush = true);

#ifdef PPR_PARALLEL_MPI
    // Default MPI message handling.
    // returns true if handled successfully.
    bool DefaultMessageHandler(const std::uint8_t* buffer, const int source);
#endif
  };
}


#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>
#include <vector>
#include <map>
#include <random>
#include <algorithm>
#include <float.h>
#include <cstdint>

#ifdef PPR_PARALLEL_TBB
#include "tbb/tbb.h"
#ifdef _WIN32
#undef max
#undef min
#endif
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#else
#define max(a,b) ((a) > (b) ? (a) : (b))
#define min(a,b) ((a) < (b) ? (a) : (b))
#endif

#ifdef PPR_PARALLEL_MPI
#include "ompi/mpi.h"
#endif

#if defined(PPR_PARALLEL_TBB) && defined(PPR_PARALLEL_MPI)
#error "Cannot compile with both flags PPR_PARALLEL_MPI and PPR_PARALLEL_TBB."
#endif

extern "C" 
{
#include "sqlite/sqlite3.h"
#include "ini/ini.h"
};

template <typename T>
inline void HARD_ASSERT(T check, const char* errMsg = "")
{
  if (check == 0)
  {
    std::cerr << "Assertation failed. " << errMsg << std::endl;
#if defined(_DEBUG) && defined(_MSC_VER)
    __debugbreak();
#endif
    exit(-1);
  }
}

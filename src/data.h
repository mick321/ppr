#pragma once
#include "defaults.h"
#include "math/spline.h"

namespace ppr
{
  // One point in time, when measurement of ist/blood occured.
  struct MeasurePoint
  {
    // primary data
    DATETIME  tim = 0;    // date+time of measurement
    REAL      ist = 0;    // concentration in int.st.liq.
    REAL      blood = 0;  // concentration in blood
    // additional data
    int       id = 0;     // identifier of data point
    int       segmentId = 0; // id of parent segment
    bool      istNotNull = false; // have we got value in variable ist?
    bool      bloodNotNull = false; // have we got value in variable blood?
  };

  // Information about subject - subject is related 1:1 to segment.
  struct MeasureSubject
  {
    std::string name;     // name of subject
    std::string comments; // comments on subject
    std::string sex;      // sex of subject
    double weight = 0;    // weight of subject, zero when undefined
    int    id = 0;        // identifier of subject
    int    segmentId = 0; // id of parent segment
  };

  // One segment of measurement, containing measure points.
  struct MeasureSegment
  {
    int id = 0; // identifier of data segment
    std::vector<MeasurePoint> points; // data points of measurement
    
    mathutils::Spline1D<REAL> valuesBlood; // spline interpolation of measurements of concentraion in blood
    mathutils::Spline1D<REAL> valuesIst; // spline interpolation of measurements of concentraion in ist.

    std::string name;       // name of segment
    MeasureSubject subject; // information about subject

    // Default constructor.
    inline MeasureSegment() { }
    // Prepare spline interpolation of dataset.
    TResult PrepareSplines();
  };

  // Data for computation (preloaded from sqlite db).
  class Data : public Generic
  {
  private:
    std::map<int, MeasureSegment> segments;

    bool initialized; // is this object properly initialized?

    // Load raw data (measurements) from sqlite db, including additional data. 
    // "filename": sqlite file
    TResult LoadRawData(const char* filename);
    // Callback defined for sqlite3_exec (static dispatcher). Here we take care of each row fetched from db.
    static int LoadRawDataCallbackSt(void* userData, int columnCount, char** columnValues, char** columnNames);
    // Callback defined for sqlite3_exec. Here we take care of each row fetched from db.
    int LoadRawDataCallback(int columnCount, char** columnValues, char** columnNames);

    // Callback defined for sqlite3_exec (static dispatcher), loading additional data about segments and subjects. Here we take care of each row fetched from db.
    static int LoadSubSegCallbackSt(void* userData, int columnCount, char** columnValues, char** columnNames);
    // Callback defined for sqlite3_exec, loading additional data about segments and subjects. Here we take care of each row fetched from db.
    int LoadSubSegCallback(int columnCount, char** columnValues, char** columnNames);
  public:
    // Constructor, "owner" is a pointer to responsible owner.
    Data(Generic* owner);

    // Load data from db file.
    // "filename": sqlite file
    TResult Load(const char* filename);
    // Release all loaded data.
    TResult Release();

    // Obtain stored map of segments.
    inline std::map<int, MeasureSegment>& Segments() { return segments; }
  };
}
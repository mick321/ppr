//
// spline interpolation
//  (well, i wrote this for tower engine before, so why to write same thing again)
//

#pragma once

namespace mathutils
{
  // Abstract function.
  template <typename DATATYPE>
  class Function
  {
  public:
    // Compute value of function.
    virtual DATATYPE GetValue(DATATYPE t) const { return (DATATYPE)0; }
    // Compute value of function.
    inline DATATYPE operator()(DATATYPE x) const { return GetValue(x); }
  };

  // Mode of spline (C0, C2, ...);
  enum TSplineMode
  {
    smC0,             // continuos segmented line
    smSmoothStep,     // continuos, derivations are zero at each point
    smC2,             // c2 continuos
    smForceDword = 0x7fffffff
  };

  // one value in spline interpolation, includes following interval
  template <typename DATATYPE>
  struct point_interval
  {
    DATATYPE x;               // value of variable
    DATATYPE fx;              // value of f(x) in x
    DATATYPE dfx_dx_left;     // derivation on interval left border
    DATATYPE dfx_dx_right;    // derivation on interval right border

    // constructor
    point_interval(const DATATYPE x, DATATYPE fx) : x(x), fx(fx), dfx_dx_left(0), dfx_dx_right(0) {}
    // computing spline value - in Bezier form using de Casteljau algorithm due to numerical stability
    // we need "currentT" as a current spline parameter, nextT as a next known border point, nextX as next known spline value
    inline DATATYPE deCasteljau(const DATATYPE currentT, const DATATYPE nextT, const DATATYPE nextX) const
    {
      const DATATYPE deltaX = (nextT - this->x);
      const DATATYPE t = (currentT - x) / deltaX;
      const DATATYPE one_minus_t = 1 - t;
      register DATATYPE x0, x1, x2, x3;

      x0 = this->fx;
      x1 = this->fx + ((DATATYPE)1 / 3) * dfx_dx_left * deltaX;
      x2 = nextX - ((DATATYPE)1 / 3) * dfx_dx_right * deltaX;
      x3 = nextX;

      x0 = one_minus_t * x0 + t * x1;
      x1 = one_minus_t * x1 + t * x2;
      x2 = one_minus_t * x2 + t * x3;

      x0 = one_minus_t * x0 + t * x1;
      x1 = one_minus_t * x1 + t * x2;

      return one_minus_t * x0 + t * x1;
    }
  };

  //
  // Spline interpolation of 1D point set.
  //
  template <typename DATATYPE = float>
  class Spline1D : public Function<DATATYPE>
  {
  private:
    // Find largest X in list of stored points lesser than argument x.
    inline int findIndex(DATATYPE x) const
    {
      int left = 0;
      int right = (int)stored_values.size()-1;
      int mid = 0;
      while (left <= right)
      {
        mid = (left + right) >> 1;
        const DATATYPE value = stored_values[mid].x;
        if (value >= x)
        {
          right = mid - 1;
        }
        else
        {
          left = mid + 1;
        }
      }
      if (mid >= 0 && stored_values[mid].x > x)
      {
        mid--;
      }
      return mid;
    }
  public:
    std::vector<point_interval<DATATYPE> > stored_values; // internal, do not use directly unless you really know what you are doing
    bool extrapolationIsConstant;  // if false, border segments continue to infinities
    TSplineMode mode;    // spline can operate in several modes

    /// Constructor.
    Spline1D()
    {
      extrapolationIsConstant = false;
    }

    /// Add new control point.
    inline void AddPoint(DATATYPE x, DATATYPE fx)
    {
      if (stored_values.size() == 0)
        stored_values.push_back(point_interval<DATATYPE>(x, fx));
      else
        stored_values.insert(stored_values.begin() + (findIndex(x) + 1), point_interval<DATATYPE>(x, fx));
    }

    /// Prepare C0 spline (polyline, no curves).
    inline void PrepareC0()
    {
      mode = smC0;
      int count1 = (int)stored_values.size() - 1;
      for (int i = 0; i < count1; i++)
      {
        stored_values[i].dfx_dx_left = (stored_values[i+1].fx - stored_values[i].fx) / (stored_values[i+1].x - stored_values[i].x);
      }
      for (int i = 0; i < count1; i++)
      {
        stored_values[i].dfx_dx_right = stored_values[i].dfx_dx_left;
      }
    }

    /// Prepare C2 spline (curve).
    inline void PrepareC2()
    {
      mode = smC2;
      const int count = (int)stored_values.size();
      if (count < 2)
        return;
      const int count1 = count - 1;
      DATATYPE* buffer = new DATATYPE[count * 5];
      HARD_ASSERT(buffer, "Memory alloc in spline returned null pointer.");
      if (!buffer)
        return;
      DATATYPE* diag = buffer;
      DATATYPE* subdiag = diag + count;
      DATATYPE* abovediag = subdiag + count;
      DATATYPE* rhs = abovediag + count;
      DATATYPE* invt = rhs + count;

      // tridiagonal matrix (indices = x coord in matrix)
      // U*
      // D U - - - -
      // S D U - - -
      // - S D U - -          (* = outside the matrix)
      //  .........
      //       S D U
      //         S D
      //           S* 
      
      for (int i = 0; i < count1; i++)
        invt[i] = /*((DATATYPE)1) /*/ (stored_values[i + 1].x - stored_values[i].x);
      
      diag[0] = (DATATYPE)2 * invt[0];
      diag[count1] = (DATATYPE)2 * invt[count1 - 1];
      subdiag[0] = invt[0];
      abovediag[count1] = invt[count1 - 1];
      rhs[0] = (DATATYPE)3 * (stored_values[1].fx - stored_values[0].fx);
      rhs[count1] = (DATATYPE)3 * (stored_values[count1].fx - stored_values[count1-1].fx);
      for (int i = 1; i < count1; i++)
      {
        diag[i] = (invt[i-1] + invt[i]) * 2;
        subdiag[i] = invt[i];
        abovediag[i] = invt[i-1];
        rhs[i] = (DATATYPE)3 * (stored_values[i+1].fx - stored_values[i-1].fx);
      }

      // gauss seidel
      for (int i = 1; i < count; i++)
      {
        DATATYPE c = -subdiag[i-1] / diag[i-1];
        subdiag[i-1] = (DATATYPE)0;
        diag[i] += c * abovediag[i];
        rhs[i] += c * rhs[i-1];
      }

      // now we have only diagonal matrix -> normalize result
      stored_values[count1].dfx_dx_left = rhs[count1] / diag[count1];
      for (int i = count1 - 1; i >= 0; i--)
      {
        stored_values[i].dfx_dx_left = (rhs[i] - stored_values[i + 1].dfx_dx_left * abovediag[i + 1]) / diag[i];
      }
      // copy derivations to neighbors
      for (int i = 0; i < count1; i++)
      {
        stored_values[i].dfx_dx_right = stored_values[i + 1].dfx_dx_left;
      }
      // release temporary data used for tridiagonal matrix
      delete []buffer;
    }

    /// Prepare spline with derivations equal to zero in control points.
    inline void PrepareSmoothStep()
    {
      // smoothstep "stops" in each control point
      mode = smSmoothStep;
      int count1 = (int)stored_values.size() - 1;
      for (int i = 0; i < count1; i++)
      {
        stored_values[i].dfx_dx_left = 0.f;
        stored_values[i].dfx_dx_right = 0.f;
      }
    }

    /// Compute value of spline in x.
    virtual DATATYPE GetValue(DATATYPE x) const
    {
      if (stored_values.size() == 0)
        return (DATATYPE)0;

      // find proper interval
      int count1 = (int)stored_values.size() - 1;
      int i;

      if (x < stored_values[0].x)
      {
        if (extrapolationIsConstant)
          return stored_values[0].fx;
        i = 0;
      }
      else if (x >= stored_values[count1].x)
      {
        if (extrapolationIsConstant || count1 == 0)
          return stored_values[count1].fx;
        i = count1 - 1;
      }
      else 
        i = findIndex(x);

      // use de Casteljau algorithm to compute value
      const auto& coeffs = stored_values[i];
      const auto& coeffsNext = stored_values[i + 1];
      return coeffs.deCasteljau(x, coeffsNext.x, coeffsNext.fx);
    }
    
    // shortcut - compute value of spline in X
    inline DATATYPE operator()(DATATYPE x) { return GetValue(x); }
  };

  typedef Spline1D<float> Spline1Df;
  typedef Spline1D<double> Spline1Dd;
}
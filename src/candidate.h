// Program.h - definition of main program class
#pragma once
#include "defaults.h"

namespace ppr
{
  class Program;

  // Class with a spin lock (and respecting copy constructor, lock is not copied).
  struct ClassWithLock
  {
#ifdef PPR_PARALLEL_TBB
    tbb::spin_mutex spinlock; // spin lock instance
#endif

    // explicit constructor
    inline ClassWithLock() 
#ifdef PPR_PARALLEL_TBB
      : spinlock()
#endif
    {
    }

    // explicit copy constructor
    inline ClassWithLock(const ClassWithLock& copyFrom) 
#ifdef PPR_PARALLEL_TBB
      : spinlock()
#endif
    {
    }

    // explicit assignment
    inline ClassWithLock& operator=(const ClassWithLock& assignTo)
    {
#ifdef PPR_PARALLEL_TBB
      spinlock.unlock();
#endif
      return *this;
    }
  };

  // Candidate (holding parameters of equation)
  // Held parameters are tweaked during computation.
  struct Candidate : ClassWithLock
  {
    // block of parameters
    REAL p, pp;
    REAL cg, cgp;
    REAL c, cp;
    REAL dt, h;
    REAL k; // "hint: should be around zero"
    REAL m, n;
    int id;

    REAL fitness;

    // fast comparison
    inline bool operator== (const Candidate& rhs) const { return id == rhs.id; }

    // Print results - best candidate parameters.
    void PrintCandidateInfo(Program* mainProgram) const;
  };
}


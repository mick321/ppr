#include "stdafx.h"
#include "computation.h"

namespace ppr
{

  // -----------------------------

  // Initialize computation above given segment.
  ComputationST::ComputationST(Generic* owner, MeasureSegment& seg, const Metric metric, const int generationMaxCount, 
    const int candidateCount, const REAL crossoverProbability, const REAL differentialWeight, const REAL mutationProbability, const long seed) : Computation(owner, seg, generationMaxCount)
  {
    this->metric = metric;
    this->crossoverProbability = (unsigned int)max((REAL)0, min((REAL)0xFFFFFFFF, 0xFFFFFFFF * crossoverProbability));
    this->differentialWeight = differentialWeight;
    this->mutationProbability = mutationProbability;
    this->random.seed(seed);
    PrepareCandidates(candidateCount, random);
  }

  // Prepare our candidates, "count" defines their count. "count" must be at least four.
  void ComputationST::PrepareCandidates(const int count, std::mt19937& random)
  {
    HARD_ASSERT(count >= 4);
    candidates.resize(count);
    int idCounter = 0;

    //static_cast<Program*>(mainProgram)->Log("Starting with:");
    for (auto& cand : candidates)
    {
      cand.p = RandReal(random, config.pmin, config.pmax);
      cand.pp = RandReal(random, config.ppmin, config.ppmax);
      cand.cg = RandReal(random, config.cgmin, config.cgmax);
      cand.cgp = RandReal(random, config.cgpmin, config.cgpmax);
      cand.c = RandReal(random, config.cmin, config.cmax);
      cand.cp = RandReal(random, config.cpmin, config.cpmax);
      cand.dt = RandReal(random, config.dtmin, config.dtmax);
      cand.h = RandReal(random, config.hmin, config.hmax);
      cand.k = RandReal(random, config.kmin, config.kmax);
      cand.m = RandReal(random, config.mmin, config.mmax);
      cand.n = RandReal(random, config.nmin, config.nmax);
      cand.id = idCounter++;
      ComputeFitness(cand);
      //cand.PrintCandidateInfo(static_cast<Program*>(mainProgram));
    }
  }

  // crossing genes (macro for generic gene).
#define CROSS_ONE_GENE(param) \
  { \
    if (RandReal(random, 0.0, 1.0) > mutationProbability) \
    { \
      double temp = CrossOneGene(x.param, a.param, b.param, c.param, random); \
      result.param = (temp < config.param##min) ? config.param##min : ((temp > config.param##max) ? config.param##max : temp); \
    }\
    else\
    {\
      result.param = RandReal(random, config.param##min, config.param##min); \
    }\
  }

  // Create new candidate, using these given four.
  // "x" is the original candidate, "a", "b", "c" are parents of "differential" child
  Candidate ComputationST::CrossCandidates(const Candidate& x, const Candidate& a, const Candidate& b, const Candidate& c, std::mt19937& random) const
  {
    Candidate result;
    CROSS_ONE_GENE(p);
    CROSS_ONE_GENE(pp);
    CROSS_ONE_GENE(cg);
    CROSS_ONE_GENE(cgp);
    CROSS_ONE_GENE(c);
    CROSS_ONE_GENE(cp);
    CROSS_ONE_GENE(dt);
    CROSS_ONE_GENE(h);
    CROSS_ONE_GENE(k);
    CROSS_ONE_GENE(m);
    CROSS_ONE_GENE(n);
    result.id = x.id;
    result.fitness = ComputeFitness(result);
    return result;
  }

  // Compute fitness function of candidate "cand".
  REAL ComputationST::ComputeFitness(Candidate& cand) const
  {
    REAL err = 0; // total metric (error)
    REAL temp; // variable used for temporary asignments
    REAL invcount = (REAL)1.0 / segment.valuesBlood.stored_values.size(); // inverse of count (normalization)
    switch (metric)
    {
    case metricAbsDif:
      for (const auto& point : segment.valuesBlood.stored_values) // only times with "blood" samples are computed
      {
        err += invcount * fabs(ComputeEqDiff(cand, point.x, segment.valuesBlood, segment.valuesIst));
      }
      break;
    case metricAbsDifSq:
      for (const auto& point : segment.valuesBlood.stored_values) // only times with "blood" samples are computed
      {
        temp = ComputeEqDiff(cand, point.x, segment.valuesBlood, segment.valuesIst);
        temp *= temp; // squared
        err += invcount * temp;
      }
      break;
    case metricMaxDif:
      for (const auto& point : segment.valuesBlood.stored_values) // only times with "blood" samples are computed
      {
        err = max(err, fabs(ComputeEqDiff(cand, point.x, segment.valuesBlood, segment.valuesIst)));
      }
      break;
    default:;
    }
    cand.fitness = err;
    return err;
  }

  // Run computation above segment.
  TResult ComputationST::Run()
  {
    unsigned int nextinfo = 1;
    REAL bestFitness = DBL_MAX; // best fitness function value
    REAL avgFitness = DBL_MAX; // average fitness function value
    REAL bestFitnessPrintedOnScreen = DBL_MAX;
    Candidate* ptrBestCandidate = &candidates[0]; // pointer to candidate with best fitness function
    REAL invCandidateCount = (REAL)1 / candidates.size();

    unsigned int lastImprovement = 0; // last generation that improved fitness function
    const unsigned int generationMaxCountNoImprovement = generationMaxCount / 20; // stop evolving if there was no improvement for this count of generations

    // -> differential evolution
    unsigned int generation = 0;
    bool errNaN = false; // did NaN error occurred?

    for (generation = 0; generation < generationMaxCount && (generation - lastImprovement) < generationMaxCountNoImprovement && bestFitness > PPR_FITNESS_THRESHOLD; generation++)
    {
      // from time to time, print information about calculation
      if (nextinfo <= generation || bestFitnessPrintedOnScreen > 100 * bestFitness)
      {
        char buffer[1024];
        sprintf(buffer, "gen. %d/%d, currently best fitness = %le, continuing...", generation, generationMaxCount, (double)bestFitness);
        static_cast<Program*>(mainProgram)->Log(buffer, true, false, false);
        nextinfo = generation + generationMaxCount / 20;
        bestFitnessPrintedOnScreen = bestFitness;
      }
      // iterate over candidates and... evolve
      avgFitness = 0;
      for (auto& current : candidates)
      {
        // pick three distinct random candidates
        Candidate* candA;
        Candidate* candB;
        Candidate* candC;
        do
        {
          candA = &candidates[random() % candidates.size()];
        } while (candA == &current);
        do
        {
          candB = &candidates[random() % candidates.size()];
        } while (candB == &current || candB == candA);
        do
        {
          candC = &candidates[random() % candidates.size()];
        } while (candC == &current || candC == candA || candC == candB);

        // crossover, newbie is born
        Candidate newbie = CrossCandidates(current, *candA, *candB, *candC, random);
        // replace if newbie is better
        if (newbie.fitness < current.fitness)
          current = newbie;

        if (current.fitness < bestFitness)
        {
          bestFitness = current.fitness;
          ptrBestCandidate = &current;
          lastImprovement = generation;
        }
        avgFitness += current.fitness;
      }
      avgFitness *= invCandidateCount;

      // prevent spreading NaN/+inf etc.
      // if bestFitness has not been assigned, NaN/+inf occurred!
      if (bestFitness == DBL_MAX)
      {
        char buffer[1024];
        sprintf(buffer, "Numerical error occurred (fitness returned not a number), no results.\nFinished after %d gen.", generation);
        static_cast<Program*>(mainProgram)->Log(buffer);
        errNaN = true;
        break;
      }
    }
    
    if (!errNaN)
    {
      char buffer[1024];
      sprintf(buffer, "Finished after %d gen., results based on %d measures of ins. in blood.", generation, (int)segment.valuesBlood.stored_values.size());
      static_cast<Program*>(mainProgram)->Log(buffer);
      ptrBestCandidate->PrintCandidateInfo(static_cast<Program*>(mainProgram));
      bestResult = *ptrBestCandidate;
    }

    return errNaN ? resFailed : resOk;
  }


  // -----------------------------------------------------------

#ifdef PPR_PARALLEL_TBB

  // Initialize computation above given segment.
  ComputationMT::ComputationMT(Generic* owner, MeasureSegment& seg, const Metric metric, const int generationMaxCount, const int candidateCount,
    const REAL crossoverProbability, const REAL differentialWeight, const REAL mutationProbability) : ComputationST(owner, seg, metric, generationMaxCount, candidateCount, crossoverProbability, differentialWeight, mutationProbability)
  {
  }

  // Run computation above segment.
  TResult ComputationMT::Run()
  {
    unsigned int nextinfo = 1;
    REAL bestFitness = DBL_MAX; // best fitness function value
    REAL avgFitness = DBL_MAX; // average fitness function value
    REAL bestFitnessPrintedOnScreen = DBL_MAX;
    Candidate* ptrBestCandidate = &candidates[0]; // pointer to candidate with best fitness function
    REAL invCandidateCount = (REAL)1 / candidates.size();

    unsigned int lastImprovement = 0; // last generation that improved fitness function
    const unsigned int generationMaxCountNoImprovement = generationMaxCount / 20; // stop evolving if there was no improvement for this count of generations

    // -> differential evolution
    unsigned int generation = 0;
    bool errNaN = false; // did NaN error occurred?
    tbb::spin_mutex spinlockBest;

    static const int GENERATION_STEP(1); // process more candidates in one bigger generation (better parallelism) - actually, the scheduler handles it well itself, so make it just one
    tbb::atomic<int> atomicThreadCounter; // atomic thread id counter
    atomicThreadCounter.store(0);

    for (generation = 0; 
      generation < generationMaxCount && (generation - lastImprovement) < generationMaxCountNoImprovement && bestFitness > PPR_FITNESS_THRESHOLD; 
      generation += GENERATION_STEP) // we process more candidates in one bigger generation (better parallelism), so in the end we need less "generations"
    {
      // from time to time, print information about calculation
      if (nextinfo <= generation || bestFitnessPrintedOnScreen > 100 * bestFitness)
      {
        char buffer[1024];
        sprintf(buffer, "gen. %d/%d, currently best fitness = %le, continuing...", generation, generationMaxCount, (double)bestFitness);
        static_cast<Program*>(mainProgram)->Log(buffer, true, false, false);
        nextinfo = generation + generationMaxCount / 10;
        bestFitnessPrintedOnScreen = bestFitness;
      }
      // iterate over candidates and... evolve

      avgFitness = 0;
      tbb::parallel_for(tbb::blocked_range<size_t>(0, GENERATION_STEP * candidates.size()),
        [&](const tbb::blocked_range<size_t>& r) 
      {
        // initialize random number generator for this thread
        ThreadData& myData = randomPool.local();
        if (!myData.initialized)
        {
          myData.random.seed(std::mt19937::default_seed + (atomicThreadCounter++));
          myData.offset = myData.random();
          myData.initialized = true;
        }
        std::mt19937& myRandom = myData.random;

        for (size_t candidateI = r.begin(); candidateI != r.end(); ++candidateI)
        {
          Candidate* currentPtr = NULL;
          do
          {
            currentPtr = &candidates[(candidateI + myData.offset) % candidates.size()];
            if (!currentPtr->spinlock.try_lock())
            {
              currentPtr = NULL;
              myData.offset = myData.random();
            }
          } while (!currentPtr);

          Candidate& current = *currentPtr;
          Candidate* candA;
          Candidate* candB;
          Candidate* candC;
          do
          {
            candA = &candidates[myRandom() % candidates.size()];
          } while (candA == currentPtr || !candA->spinlock.try_lock());
          do
          {
            candB = &candidates[myRandom() % candidates.size()];
          } while (candB == currentPtr || candB == candA || !candB->spinlock.try_lock());
          do
          {
            candC = &candidates[myRandom() % candidates.size()];
          } while (candC == currentPtr || candC == candA || candC == candB || !candC->spinlock.try_lock());

          Candidate newbie = CrossCandidates(current, *candA, *candB, *candC, myRandom);
          if (newbie.fitness < current.fitness)
          {
            current = newbie;
          }

          // critical section: assign "best" candidate
          {
            tbb::spin_mutex::scoped_lock lock(spinlockBest);
            if (current.fitness < bestFitness)
            {
              bestFitness = current.fitness;
              ptrBestCandidate = &current;
              lastImprovement = generation;
            }
            avgFitness += current.fitness;
          }

          // unlock our chosen candidates
          currentPtr->spinlock.unlock();
          candA->spinlock.unlock();
          candB->spinlock.unlock();
          candC->spinlock.unlock();
        }
      });

      avgFitness *= invCandidateCount;

      // prevent spreading NaN/+inf etc.
      // if bestFitness has not been assigned, NaN/+inf occurred!
      if (bestFitness == DBL_MAX)
      {
        char buffer[1024];
        sprintf(buffer, "Numerical error occurred (fitness returned not a number), no results.\nFinished after %d gen.", generation);
        static_cast<Program*>(mainProgram)->Log(buffer);
        errNaN = true;
        break;
      }
    }

    if (!errNaN)
    {
      char buffer[1024];
      sprintf(buffer, "Finished after %d gen., results based on %d measures of ins. in blood.", generation, (int)segment.valuesBlood.stored_values.size());
      static_cast<Program*>(mainProgram)->Log(buffer);
      ptrBestCandidate->PrintCandidateInfo(static_cast<Program*>(mainProgram));
      bestResult = *ptrBestCandidate;
    }

    return errNaN ? resFailed : resOk;
  }

#endif

  // code specific for MPI
#ifdef PPR_PARALLEL_MPI

  // Initialize computation above given segment.
  ComputationMPIMaster::ComputationMPIMaster(Generic* owner, MeasureSegment& seg, const Metric metric, const int candidateCount)
    : ComputationMPI(owner, seg, metric, 0, candidateCount, 0, 0, 0)
  {
    HARD_ASSERT(candidateCount > 0);
  }

  // Try to receive best candidates from any slave (receive best, merge them with our stored ones).
  bool ComputationMPIMaster::TryReceiveBest(int& refNumFinished)
  {
    const int maxBufferSize = 1024 * 32;
    std::uint8_t buffer[maxBufferSize];
    int size, source;
    if (MPI_GenericReceive(mainProgram, buffer, maxBufferSize, size, source, true))
    {
      std::int32_t messageIdentifier;
      memcpy(&messageIdentifier, buffer + 0, 4);
      switch (messageIdentifier)
      {
      case PPR_MPI_MSG_BEST:
        ReceiveBest(buffer, source);
        break;
      case PPR_MPI_MSG_FINISHED:
        {
          std::int32_t segId;
          memcpy(&segId, buffer + 4, 4);
          if (segId == static_cast<Program*>(mainProgram)->countProcessed)
          {
            sprintf((char*)buffer, "Slave %d ran out of iterations and finished.", source);
            static_cast<Program*>(mainProgram)->Log((char*)buffer);
          }
          ++refNumFinished;
        }
        break;
      default:
        // default handler
        static_cast<Program*>(mainProgram)->DefaultMessageHandler(buffer, source);
      }
    }
    return true;
  }

  // Receive best candidates from slave (parse them out of buffer, merge them with our stored ones).
  void ComputationMPIMaster::ReceiveBest(const std::uint8_t* buffer, int slaveId)
  {
    std::int32_t segId;
    memcpy(&segId, buffer + 4, 4);
    if (segId != static_cast<Program*>(mainProgram)->countProcessed) // check if not old
      return;
    std::int32_t receivedCount;
    memcpy(&receivedCount, buffer + 8, 4);
    const std::uint8_t* bufferPtr = buffer + 12;
    for (int i = 0; i < receivedCount; i++, bufferPtr += sizeof(Candidate))
    {
      const Candidate* candNew = reinterpret_cast<const Candidate*>(bufferPtr);

      // now find current worst and replace it when we got better one
      Candidate* worstPtr = &candidates[0];
      for (Candidate& cand : candidates)
      {
        if (cand.fitness > worstPtr->fitness)
          worstPtr = &cand;
      }

      if (worstPtr->fitness > candNew->fitness)
      {
        *worstPtr = *candNew; // replace it
      }
    }

    // update best
    REAL bestFitness = DBL_MAX;
    Candidate* ptrBest = NULL;
    for (Candidate& cand : candidates)
    {
      if (cand.fitness < bestFitness)
      {
        bestFitness = cand.fitness;
        ptrBest = &cand;
      }
    }
    if (ptrBest)
      bestResult = *ptrBest;
  }

  // Send to every slave that master has finished.
  void ComputationMPIMaster::SpamMasterFinished()
  {
    int numNodes;
    std::int32_t sendBuffer[2];
    sendBuffer[0] = PPR_MPI_MSG_FINISHED;
    sendBuffer[1] = static_cast<Program*>(mainProgram)->countProcessed; // identifier of computation
    MPI_Comm_size(MPI_COMM_WORLD, &numNodes);
    for (int i = 1; i < numNodes; i++)
      MPI_Send(sendBuffer, 8, MPI_BYTE, i, PPR_MPI_TAG, MPI_COMM_WORLD);
  }

  // Run computation above segment.
  TResult ComputationMPIMaster::Run()
  {
    sendBuffer = NULL;
    REAL bestFitness = DBL_MAX; // best fitness function value
    REAL bestFitnessPrintedOnScreen = DBL_MAX;

    int numFinished = 0;
    int numSlaves;
    MPI_Comm_size(MPI_COMM_WORLD, &numSlaves);
    --numSlaves;

    int iterations = 0;
    bestResult.fitness = DBL_MAX;
    // -> differential evolution
    while (bestFitness > PPR_FITNESS_THRESHOLD && numFinished < numSlaves)
    {
      // from time to time, print information about calculation
      if (bestFitnessPrintedOnScreen > 100 * bestFitness)
      {
        char buffer[1024];
        sprintf(buffer, "working slaves: %d/%d, currently best fitness = %le, continuing...", numSlaves - numFinished, numSlaves, (double)bestFitness);
        static_cast<Program*>(mainProgram)->Log(buffer, true, false, false);
        bestFitnessPrintedOnScreen = bestFitness;
      }

      TryReceiveBest(numFinished);
      bestFitness = bestResult.fitness;
      iterations++;
    }

    SpamMasterFinished();

    {
      char buffer[1024];
      sprintf(buffer, "Finished after %d iterations (exchanges),\nresults based on %d measures of ins. in blood.", iterations, (int)segment.valuesBlood.stored_values.size());
      static_cast<Program*>(mainProgram)->Log(buffer);
      bestResult.PrintCandidateInfo(static_cast<Program*>(mainProgram));
    }
    if (sendBuffer)
    {
      MPI_Wait(&sendRequest, MPI_STATUS_IGNORE);
      free(sendBuffer);
      sendBuffer = NULL;
    }
    return resOk;
  }

  // ---------------------------------------------------------------------------------------------------

  // Initialize computation above given segment.
  ComputationMPISlave::ComputationMPISlave(Generic* owner, MeasureSegment& seg, const Metric metric, const int generationMaxCount, const int candidateCount,
    const REAL crossoverProbability, const REAL differentialWeight, const REAL mutationProbability)
    : ComputationMPI(owner, seg, metric, generationMaxCount, candidateCount, crossoverProbability, differentialWeight, mutationProbability)
  {
    HARD_ASSERT(candidateCount > 0);
    MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
    MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);
  }

  // Run computation above segment.
  TResult ComputationMPISlave::Run()
  {
    std::mt19937 random; // random number generator
    REAL bestFitness = DBL_MAX; // best fitness function value

    sendBuffer = NULL;
    sendRequestToSlaveValid = false;

    unsigned int lastImprovement = 0; // last generation that improved fitness function
    const unsigned int generationMaxCountNoImprovement = generationMaxCount / 20; // stop evolving if there was no improvement for this count of generations

    // -> differential evolution
    unsigned int generation = 0;
    bool errNaN = false; // did NaN error occurred?

    // set different seed for each slave
    {
      int rank;
      MPI_Comm_rank(MPI_COMM_WORLD, &rank);
      random.seed(std::mt19937::default_seed + 4321 * rank);
    }
    // prepare candidates using our seed
    PrepareCandidates((int)candidates.size(), random);

    masterFinished = false;

    const int enchangePeriod = 40;
    unsigned int nextCandidateExchange = enchangePeriod + random() % enchangePeriod; // number of generation when communication with master will occur
    
    for (generation = 0; generation < generationMaxCount && (generation - lastImprovement) < generationMaxCountNoImprovement && bestFitness > PPR_FITNESS_THRESHOLD; generation++)
    {
      // iterate over candidates and... evolve
      for (auto& current : candidates)
      {
        // pick three distinct random candidates
        Candidate* candA;
        Candidate* candB;
        Candidate* candC;
        do
        {
          candA = &candidates[random() % candidates.size()];
        } while (candA == &current);
        do
        {
          candB = &candidates[random() % candidates.size()];
        } while (candB == &current || candB == candA);
        do
        {
          candC = &candidates[random() % candidates.size()];
        } while (candC == &current || candC == candA || candC == candB);

        // crossover, newbie is born
        Candidate newbie = CrossCandidates(current, *candA, *candB, *candC, random);
        // replace if newbie is better
        if (newbie.fitness < current.fitness)
          current = newbie;

        if (current.fitness < bestFitness)
        {
          bestFitness = current.fitness;
          lastImprovement = generation;

          if (bestFitness <= PPR_FITNESS_THRESHOLD)
            nextCandidateExchange = generation;
        }
      }

      // prevent spreading NaN/+inf etc.
      // if bestFitness has not been assigned, NaN/+inf occurred!
      if (bestFitness == DBL_MAX)
      {
        char buffer[1024];
        sprintf(buffer, "Numerical error occurred (fitness returned not a number), no results.\nFinished after %d gen.", generation);
        static_cast<Program*>(mainProgram)->Log(buffer);
        errNaN = true;
        break;
      }
      else
      {
        if (generation >= nextCandidateExchange)
        {
          //static_cast<Program*>(mainProgram)->Log("exchanging...", true, false, false);
          SubmitMyBest();
          if (masterFinished)
            break;
          nextCandidateExchange += enchangePeriod;
        }

        ReceiveMasterBest(); // try to receive data from master
      }
    }

    if (sendBuffer)
    {
      if (masterFinished)
      {
        MPI_Cancel(&sendRequest);
      }
      MPI_Wait(&sendRequest, MPI_STATUS_IGNORE);
      if (sendRequestToSlaveValid)
      {
        if (masterFinished)
        {
          MPI_Cancel(&sendRequestToSlave);
        }
        MPI_Wait(&sendRequestToSlave, MPI_STATUS_IGNORE);
        sendRequestToSlaveValid = false;
      }
      free(sendBuffer);
      sendBuffer = NULL;
    }

    if (!masterFinished)  // do not send if not necessary (masterFinished? then i was interrupted, no need to inform him back)
    {
      SendFinished();
    }

    return errNaN ? resFailed : resOk;
  }

  // Send my best candidates to master and random slave.
  void ComputationMPISlave::SubmitMyBest()
  {
    const int sendCount = min((int)candidates.size(), 3);
    const int sendBufferSize = 4 + 4 + 4 + sendCount * sizeof(Candidate);
    if (sendBuffer)
    {
      MPI_Wait(&sendRequest, MPI_STATUS_IGNORE);

      if (sendRequestToSlaveValid)
      {
        MPI_Wait(&sendRequestToSlave, MPI_STATUS_IGNORE);
        sendRequestToSlaveValid = false;
      }
    }
    else
    {
      sendBuffer = (std::uint8_t*)malloc(sendBufferSize);
    }

    if (!sendBuffer)
    {
      static_cast<Program*>(mainProgram)->Log("Error: cannot allocate memory.");
      return;
    }

    std::sort(candidates.begin(), candidates.end(), [](const Candidate& lhs, const Candidate& rhs) { return lhs.fitness < rhs.fitness; });

    // create datagram
    int32_t temp = PPR_MPI_MSG_BEST;
    memcpy(sendBuffer + 0, &temp, 4);
    temp = static_cast<Program*>(mainProgram)->countProcessed;
    memcpy(sendBuffer + 4, &temp, 4);
    temp = sendCount;
    memcpy(sendBuffer + 8, &temp, 4);
    std::uint8_t* sendBufferPtr = sendBuffer + 12;
    // best
    for (int i = 0; i < sendCount; i++)
    {
      Candidate& cand = candidates[i];
      memcpy(sendBufferPtr, &cand, sizeof(Candidate));
      sendBufferPtr += sizeof(Candidate);
    }

    // send it!
    MPI_Isend(sendBuffer, sendBufferSize, MPI_BYTE, 0, PPR_MPI_TAG, MPI_COMM_WORLD, &sendRequest);
    int sendToSlave;
    while (worldSize > 2 && worldRank == (sendToSlave = 1 + GetComputationRandom()() % (worldSize - 1)))
    {
      MPI_Isend(sendBuffer, sendBufferSize, MPI_BYTE, sendToSlave, PPR_MPI_TAG, MPI_COMM_WORLD, &sendRequestToSlave);
      sendRequestToSlaveValid = true;
    }
  }

  // Receive master's best candidates.
  void ComputationMPISlave::ReceiveMasterBest()
  {
    const int maxBufferSize = 1024 * 32;
    std::uint8_t buffer[maxBufferSize];
    int size, source;
    while (MPI_GenericReceive(mainProgram, buffer, maxBufferSize, size, source, false))
    {
      std::int32_t messageIdentifier;
      memcpy(&messageIdentifier, buffer, 4);
      switch (messageIdentifier)
      {
      case PPR_MPI_MSG_BEST:
        ExchangeBest(buffer);
        break;
      case PPR_MPI_MSG_FINISHED:
        {
          std::int32_t segId;
          memcpy(&segId, buffer + 4, 4); //check if it is same segment
          if (segId == static_cast<Program*>(mainProgram)->countProcessed)
          {
            masterFinished = true; // master has finished, my work has no sense anymore...
          }
        }
        break;
      default:
        // default handler
        static_cast<Program*>(mainProgram)->DefaultMessageHandler(buffer, source);
        break;
      }
    }
  }

  // Exchange best candidates with slave (parse them out of buffer, merge them with our stored ones).
  void ComputationMPISlave::ExchangeBest(const std::uint8_t* buffer)
  {
    std::int32_t segId;
    memcpy(&segId, buffer + 4, 4);
    if (segId != static_cast<Program*>(mainProgram)->countProcessed) // check if not old
      return;
    std::int32_t receivedCount;
    memcpy(&receivedCount, buffer + 8, 4);
    const std::uint8_t* bufferPtr = buffer + 12;
    for (int i = 0; i < receivedCount; i++, bufferPtr += sizeof(Candidate))
    {
      const Candidate* candNew = reinterpret_cast<const Candidate*>(bufferPtr);

      // now choose one and replace it when we got better one
      Candidate* candPtr = &candidates[GetComputationRandom()() % candidates.size()];

      //if (RandReal(GetComputationRandom(), 0, 1) <= crossoverProbability)
      {
        // crossing!
        Candidate candCrossed = CrossCandidates(*candPtr, *candNew, *candNew, *candNew, GetComputationRandom());
        if (candCrossed.fitness < candPtr->fitness)
          *candPtr = candCrossed;
      }
      //else
      //{
      //  // just replace it
      //  if ((*candNew).fitness < candPtr->fitness)
      //    *candPtr = *candNew;
      //}
    }
  }

  // Inform master that slave has finished his work.
  void ComputationMPISlave::SendFinished()
  {
    if (bestResult.fitness <= PPR_FITNESS_THRESHOLD)
      SubmitMyBest();
    std::int32_t sendBuffer[2];
    sendBuffer[0] = PPR_MPI_MSG_FINISHED;
    sendBuffer[1] = static_cast<Program*>(mainProgram)->countProcessed; // identifier of computation
    MPI_Send(sendBuffer, 8, MPI_BYTE, 0, PPR_MPI_TAG, MPI_COMM_WORLD);
  }

#endif

}

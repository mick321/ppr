// Program.h - definition of main program class
#pragma once
#include "defaults.h"
#include "data.h"
#include "program.h"
#include "candidate.h"

namespace ppr
{
  // Enumeration of possible error metrics.
  enum Metric 
  { 
    metricUnknown,
    metricAbsDif,   // total of absolute differences
    metricAbsDifSq, // total of absolute squared differences
    metricMaxDif,    // maximum absolute difference
    metricEnumCount
  };

  // Enumeration of possible error metrics.
  static inline const char* GetMetricName(const Metric metric)
  {
    switch (metric)
    {
    case metricAbsDif: return "AbsDif";
    case metricAbsDifSq: return "AbsDifSq";
    case metricMaxDif: return "MaxDif";
    default: return "Unknown";
    }
  };

  // Computation of best constants for given relation between concentration in ist. and concentration in blood.
  // Computation is implemented as a differential evolution.
  class Computation : public Generic
  {
  private:
  protected:
    MeasureSegment& segment;    // reference to processed segment
    Config& config;             // reference to program configuration (constants constraints)
    const unsigned int generationMaxCount;  // maximum number of generations
    Candidate bestResult;

    // Compute difference of left and right side of our equation.
    inline static const REAL ComputeEqDiff(const Candidate& c, const REAL t, const mathutils::Function<REAL>& B_t, const mathutils::Function<REAL>& I_t)
    {
      // value of function I(t) in point t
      const REAL value_I_for_t = I_t(t);
      // value of function B(t) in point t
      const REAL value_B_for_t = B_t(t);
      // eq (1) from problem definition
      const REAL eq_phi_t = (c.h * c.h < PPR_EPS * PPR_EPS) ? 0 : (value_I_for_t - I_t(t - c.h)) / c.h;
      // eq (2) from  problem definition
      const REAL eq_psi_t = value_B_for_t * (value_B_for_t - value_I_for_t);
      // eq (3) from  problem definition
      const REAL eq_I_t = c.p * value_B_for_t + c.cg * eq_psi_t + c.c;
      // eq (4) from  problem definition
      const REAL eq_Theta_t = eq_phi_t * (c.pp * value_B_for_t + c.cgp * eq_psi_t + c.cp);
      // lhs of eq (5) from  problem definition
      const REAL lhs = eq_I_t + eq_Theta_t;
      // rhs of eq (5) from  problem definition
      const REAL rhs = c.m * I_t(t + c.dt + c.k * eq_phi_t) + c.n * I_t(t + c.dt);
      // return difference
      return lhs - rhs;
    }

  public:
    // Initialize computation above given segment.
    inline Computation(Generic* owner, MeasureSegment& seg, const int generationMaxCount) :
      Generic(owner), 
      segment(seg), 
      config(static_cast<Program*>(mainProgram)->config),
      generationMaxCount(generationMaxCount) { }
    // Run computation above segment.
    virtual TResult Run() = 0;

    // Returns best found result.
    inline const Candidate& BestResult() const {
      return bestResult;
    }
  };

  // Computation of best constants for given relation between concentration in ist. and concentration in blood.
  // Computation is implemented as a differential evolution.
  // Single thread implementation.
  class ComputationST : public Computation
  {
  private:
    std::mt19937 random;
    Metric metric; // used metric
    
    // Cross only one gene.
    inline const REAL CrossOneGene(const REAL& valueX, const REAL& valueA, const REAL& valueB, const REAL& valueC, std::mt19937& random) const
    {
      if (random() < crossoverProbability)
      {
        return valueA + differentialWeight * (valueB - valueC);
      }
      else
      {
        return valueX;
      }
    }
    // Compute fitness function of candidate "cand".
    REAL ComputeFitness(Candidate& cand) const;
  protected:
    REAL differentialWeight; // differential weight in diff. evolution, "drift"
    unsigned int crossoverProbability; // probality of using crossover value instead of original one, stored on 32bit int interval instead of [0,1]
    REAL mutationProbability;

    std::vector<Candidate> candidates; // array of candidates containing parameters

    // Prepare our candidates, "count" defines their count. "count" must be at least four.
    void PrepareCandidates(const int count, std::mt19937& random);
    // Create new candidate, using these given four.
    // "x" is the original candidate, "a", "b", "c" are parents of "differential" child
    Candidate CrossCandidates(const Candidate& x, const Candidate& a, const Candidate& b, const Candidate& c, std::mt19937& random) const;

    // Obtain "global" random number generator
    inline std::mt19937& GetComputationRandom()
    {
      return random;
    }
  public:
    // Initialize computation above given segment.
    ComputationST(Generic* owner, MeasureSegment& seg, const Metric metric, const int generationMaxCount = 100000, const int candidateCount = 30, 
      const REAL crossoverProbability = 0.65, const REAL differentialWeight = 0.7, const REAL mutationProbability = 0.1, const long seed = std::mt19937::default_seed);
    // Run computation above segment.
    virtual TResult Run();
  };

  // code specific for intel threading building blocks
#ifdef PPR_PARALLEL_TBB
  // Multithreaded implementation of computation using Intel TBB library.
  class ComputationMT : public ComputationST
  {
    struct ThreadData
    {
      std::mt19937 random;
      bool initialized = false;
      int offset;
    };
    typedef tbb::enumerable_thread_specific< ThreadData > RandomGenLT;
    RandomGenLT randomPool; // local random number generator for each thread
  public:
    // Initialize computation above given segment.
    ComputationMT(Generic* owner, MeasureSegment& seg, const Metric metric, const int generationMaxCount = 100000, const int candidateCount = 30,
      const REAL crossoverProbability = 0.65, const REAL differentialWeight = 0.7, const REAL mutationProbability = 0.1);
    // Run computation above segment.
    virtual TResult Run();
  };

#endif

  // code specific for MPI
#ifdef PPR_PARALLEL_MPI

  // MPI "computation", abstract class.
  class ComputationMPI : public ComputationST
  {
  private:
  public:
    // Initialize computation above given segment.
    inline ComputationMPI(Generic* owner, MeasureSegment& seg, const Metric metric, const int generationMaxCount, const int candidateCount,
      const REAL crossoverProbability, const REAL differentialWeight, const REAL mutationProbability)
      : ComputationST(owner, seg, metric, generationMaxCount, candidateCount, crossoverProbability, differentialWeight, mutationProbability) { }
    // Run computation above segment.
    virtual TResult Run() = 0;
  };

  // Master "computation", gathering and resending information from/to/between slaves.
  class ComputationMPIMaster : public ComputationMPI
  {
  private:
    std::mt19937 random;
    std::uint8_t* sendBuffer = NULL;
    MPI_Request sendRequest;
    // Try to receive best candidates from any slave (receive best, merge them with our stored ones).
    bool TryReceiveBest(int& refNumFinished);
    // Receive best candidates from slave (parse them out of buffer, merge them with our stored ones).
    void ReceiveBest(const std::uint8_t* buffer, int slaveId);
    // Send to every slave that master has finished.
    void SpamMasterFinished();
  public:
    // Initialize computation above given segment.
    // candidateCount = count of interchanged best candidates
    ComputationMPIMaster(Generic* owner, MeasureSegment& seg, const Metric metric, const int candidateCount = 5);
    // Run computation above segment.
    virtual TResult Run();
  };

  // Slave computation, actually computing differential evolution.
  class ComputationMPISlave : public ComputationMPI
  {
  private:
    bool masterFinished; // ah, master finished? no need to continue with my computing...
    std::uint8_t* sendBuffer = NULL;
    MPI_Request sendRequest;
    MPI_Request sendRequestToSlave;
    bool sendRequestToSlaveValid = false;
    int worldSize, worldRank; // number of nodes, rank number of this computation

    // Send my best candidates to master and random slave.
    void SubmitMyBest();
    // Receive master's best candidates.
    void ReceiveMasterBest();
    // Exchange best candidates with slave (parse them out of buffer, merge them with our stored ones).
    void ExchangeBest(const std::uint8_t* buffer);
    // Inform master that slave has finished his work.
    void SendFinished();
  public:
    // Initialize computation above given segment.
    ComputationMPISlave(Generic* owner, MeasureSegment& seg, const Metric metric, const int generationMaxCount = 100000, const int candidateCount = 15,
      const REAL crossoverProbability = 0.7, const REAL differentialWeight = 0.7, const REAL mutationProbability = 0.2);
    // Run computation above segment.
    virtual TResult Run();
  };

#endif
    
}


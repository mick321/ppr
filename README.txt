Napoveda - adresarova struktura a soubory:

bin*/ppr             - spustitelne soubory, bin_x64*, bin_x86* jsou binarky pro Windows
data/                - datove soubory (konfigurace + db)
doc/dokumentace.pdf  - dokumentace
lib/                 - knihovny (lib+dll)
src/                 - zdrojove kody
tmp/                 - slozka pro docasne soubory
visual-studio        - solution+projekt visual studia 2013
makefile*            - soubory pro preklad pomoci nastroje make
CC=g++
CFLAGS=-c -Wall -O2 -x c
CPPFLAGS=-c -Wall -O2 -std=c++11
CINCLUDES=-Isrc/ -Isrc/math -Isrc/sqlite -Isrc/ini
LDFLAGS=-pthread -ldl
SOURCESCPP=src/candidate.cpp src/computation.cpp src/data.cpp src/defaults.cpp src/main.cpp src/program.cpp src/stdafx.cpp src/math/spline.cpp
OBJECTSCPP=$(SOURCESCPP:.cpp=.o)
SOURCES=src/ini/ini.c src/sqlite/sqlite3.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=bin/ppr

all: $(SOURCES) $(SOURCESCPP) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) $(OBJECTSCPP) 
	$(CC) $(LDFLAGS) $(OBJECTS) $(OBJECTSCPP) -o $@

.cpp.o:
	$(CC) $(CINCLUDES) $(CPPFLAGS) $< -o $@
	
.c.o:
	$(CC) $(CINCLUDES) $(CFLAGS) $< -o $@
	
clean:
	cd src; find . -name "*.o" | xargs rm 
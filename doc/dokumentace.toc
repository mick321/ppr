\select@language {czech}
\contentsline {section}{\numberline {1}Zad\'an\IeC {\'\i }}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Verze \'ulohy}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Teoretick\'e pozad\IeC {\'\i }}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Meze parametr\r u}{2}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}Data}{3}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Program}{3}{subsection.1.5}
\contentsline {section}{\numberline {2}Rozbor zad\'an\IeC {\'\i }}{3}{section.2}
\contentsline {section}{\numberline {3}\v Re\v sen\IeC {\'\i } d\IeC {\'\i }l\v c\IeC {\'\i }ch probl\'em\r u}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Kubick\'y spline}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Na\v cten\IeC {\'\i } konfigurace mez\IeC {\'\i }}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Na\v cten\IeC {\'\i } dat z~SQLite datab\'aze}{4}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Diferenci\'aln\IeC {\'\i } evoluce}{5}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Architektura}{6}{subsection.3.5}
\contentsline {section}{\numberline {4}Paralelizace (sd\IeC {\'\i }len\'a pam\v e\v t)}{6}{section.4}
\contentsline {section}{\numberline {5}Paralelizace (distribuovan\'a pam\v e\v t)}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Metoda}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Realizace}{8}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Pou\v zit\'e zpr\'avy}{8}{subsection.5.3}
\contentsline {section}{\numberline {6}Nam\v e\v ren\'e a~vypo\v cten\'e charakteristiky}{9}{section.6}
\contentsline {subsection}{\numberline {6.1}Doba b\v ehu v\'ypo\v ctu}{9}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Odvozen\'e metriky}{10}{subsection.6.2}
\contentsline {section}{\numberline {7}U\v zivatelsk\'y manu\'al}{11}{section.7}
\contentsline {subsection}{\numberline {7.1}P\v reklad na syst\'emu Windows}{11}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}P\v reklad na syst\'emu Linux}{12}{subsection.7.2}
\contentsline {subsection}{\numberline {7.3}B\v eh programu}{12}{subsection.7.3}
\contentsline {section}{\numberline {8}Z\'av\v er}{12}{section.8}
